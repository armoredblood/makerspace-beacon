/* 
 **  IoT Makerspace Beacon
 **  Posts an alert to slack
 **  Written by Aaron Peterson
 **  March 13, 2019
 */

#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>

String slackBotName = "Esp8266";

bool makerspaceOpen = false;
bool alertSent = false;
bool ledOn = false;

int switchPin = 5;
int ledPin = 2;
int ledTimer = 0;
int alertThreshold = 3000; // ms. used for debounce or "oops didn't mean to turn it on"
int ledBlinkSpeed = 1000;
int alertTimer = 0;

const char * ssid = "";
const char * password = "";
const char * webhook_url = ""; 
const char * host = "hooks.slack.com";
const int httpsPort = 443;

void setup() {
  delay(2000);
  Serial.begin(115200);
  pinMode(switchPin, INPUT_PULLUP);
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);
}

void loop() {
  bool switchON = !(digitalRead(switchPin)); // input_pullup causes logic to be reversed, this fixes it
  int currentTime = millis();

  if (switchON == false && makerspaceOpen == true) {
    Serial.println(switchON);
    alertTimer = currentTime + alertThreshold;
    makerspaceOpen = false; //setting this prevents updating the alertTimer
  }

  if (switchON == true && makerspaceOpen == false) {
    Serial.println(switchON);
    alertTimer = currentTime + alertThreshold;
    makerspaceOpen = true; //setting this prevents updating the alertTimer
  }

  if (switchON == false && makerspaceOpen == false && alertSent == true && currentTime > alertTimer) {
    Serial.println("SENDING DOOR CLOSED");
    sendToSlack(":disappointed: The space is closed. :sob:");
    alertTimer = 0;
    alertSent = false;
    digitalWrite(ledPin, LOW);
  }
  if (switchON == true && makerspaceOpen == true && alertSent == false && currentTime > alertTimer) {
    Serial.println("SENDING DOOR OPEN");
    sendToSlack(":tada: The space is open! :confetti_ball:");
    alertTimer = 0;
    alertSent = true;
    ledTimer = currentTime + ledBlinkSpeed;
  }

  // light the beacon!
  if (alertSent == true && currentTime > ledTimer) {
    if (ledOn == true) {
      digitalWrite(ledPin, LOW);
      ledOn = false;
      ledTimer = currentTime + ledBlinkSpeed;
    } else {
      digitalWrite(ledPin, HIGH);
      ledOn = true;
      ledTimer = currentTime + ledBlinkSpeed;
    }

  }
}

bool sendToSlack(String msg) {
  wifiConnect();

  // Use WiFiClientSecure class to create TLS connection
  WiFiClientSecure client;
  Serial.print("connecting to ");
  Serial.println(host);

  Serial.printf("Using fingerprint '%s'\n", fingerprint);
  client.setInsecure();

  if (!client.connect(host, httpsPort)) {
    Serial.println("connection failed");
    return false;
  }

  String postData = "{\"username\": \"" + slackBotName + "\", \"text\": \"" + msg + "\"}";

  int dataLength = postData.length();

  String POST = "POST " + String(webhook_url) + " HTTP/1.1\r\n"
  "Host: " + host + "\r\n"
  "User-Agent: ArduinoIoT/1.0\r\n"
  "Connection: close\r\n"
  "Content-Type: application/x-www-form-urlencoded\r\n"
  "Content-Length: " + dataLength + "\r\n\r\n"
  "" + postData;

  Serial.println("Writing POST request to client..");
  Serial.println();
  client.print(POST);

  delay(500);

  while (client.available()) {
    Serial.println(client.readStringUntil('\r'));
  }

  delay(500);
  Serial.println("POST request complete!");
  WiFi.disconnect();
  return true;
}

void wifiConnect() {
  WiFi.disconnect();

  Serial.print("connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}
